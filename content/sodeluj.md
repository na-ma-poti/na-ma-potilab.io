---
title: "Kako lahko sodelujem?"
date: 2019-12-10
draft: false
tags: []
categories: []
---

Pridruži se nam pri naravoslovno matematičnem opismenjevanju.

<!--more-->

Sodeluješ lahko na različne načine.

## Gitlab

Gitlab je spletno sodelovalno okolje, ki omogoča skupno upravljanje spletnih strani.

* [Zahtevki](https://gitlab.com/na-ma-poti/na-ma-poti.gitlab.io/issues) omogočajo usmerjeno komunikacijo med uporabniki in avtorji spletne strani
* [Vsebina spletnih strani](https://gitlab.com/na-ma-poti/na-ma-poti.gitlab.io/tree/master/content)