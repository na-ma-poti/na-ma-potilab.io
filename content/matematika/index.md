---
title: "Matematika"
date: 2019-09-10T05:35:43+02:00
draft: false
tags: [vsakdan]
categories: [ matematika ]
---
Matematična pismenost je zmožnost posameznika, da na osnovi matematičnega mišljenja in matematičnega znanja 

* zmore uporabljati matematične pojme, postopke in orodja v različno strukturiranih okoljih 
* analizira, utemeljuje in učinkovito sporoča svoje zamisli in rezultate pri oblikovanju, reševanju in interpretaciji matematičnih problemov v različno strukturiranih okoljih 
* zaznava in se zaveda vloge matematike v vsakdanjem in poklicnem življenju, jo povezuje z  drugimi področji in sprejema odgovorne odločitve na osnovi matematičnega znanja ter je pripravljen sprejemati in soustvarjati zanj nova matematična spoznanja

<!--more-->

## Matematika v kuhinji

![torta](torta.jpg)

    Recept za biskvit: 5 jajc, 150g moke, 120g sladkorja. 
    
    Joj, imam le 3 jajca, koliko moke in sladkorja naj dam? 
    Bom vprašala na forumu! Ali pa izračunam sama, 
    saj smo delali nekaj podobnega v šoli.
