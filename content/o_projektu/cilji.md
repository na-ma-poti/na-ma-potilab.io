---
title: "Glavni cilji"
date: 2019-12-04T14:46:55+01:00
draft: false
tags: []
categories: []
---


Cilj projekta je razviti in preizkusiti pedagoške pristope in strategije oz. prožne oblike učenja, ki bodo tudi z vključevanjem novih tehnologij pripomogle k celostnemu in kontinuiranemu vertikalnemu razvoju naravoslovne, matematične in drugih pismenosti (finančne, digitalne, medijske …) otrok/učencev/dijakov od vrtcev do srednjih šol.
