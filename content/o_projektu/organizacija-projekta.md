---
title: "Organizacija projekta"
date: 2019-12-04T14:47:13+01:00
draft: false
tags: []
categories: []
---
 V projektu delujejo strateški, razvojna in delovni timi, v katerih sodelujejo predstavniki ZRSŠ, fakultet, ravnatelji ter strokovni delavci VIZ.

* Strateški tim 	

    predstavniki ZRSŠ
    predstavniki fakultet
    predstavniki VIZ – ravnatelji razvojnih VIZ,

* Razvojni tim 	

    predstavniki ZRSŠ
    predstavniki fakultet
    predstavniki strokovnih delavcev razvojnih VIZ.

* Delovni timi 	

    predstavniki fakultet
    predstavniki ZRSŠ
    predstavniki strokovnih delavcev razvojnih VIZ
