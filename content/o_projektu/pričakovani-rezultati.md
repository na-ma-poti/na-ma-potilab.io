---
title: "Pričakovani rezultati"
date: 2019-12-04T14:47:44+01:00
draft: false
tags: []
categories: []
---
Pričakovani rezultati

 *   poročila o preizkušenih primerih dobre prakse, razvitih didaktičnih pristopih in pedagoških strategijah, ki prispevajo k razvoju kritičnega mišljenja in reševanja problemov z vidika naravoslovne in matematične pismenosti,
 *   razviti in pripravljeni vertikalni izvedbeni kurikuli VIZ za naravoslovno in matematično pismenost (z vključeno finančno pismenostjo) s strategijami prožnih oblik učenja
 *   metodologija in instrumentarij za spremljanje in merjenje napredka elementov naravoslovne in matematične pismenosti.
 *   izvedena spremljava in evalvacija napredka naravoslovne in matematične pismenosti učencev ter priprava evalvacijskih poročil o napredku kompetenc.
 *   priporočilo za razvoj naravoslovne in matematične pismenosti v VIZ po vertikali.
 *   vzpostavljeno sodelovalno timsko delo na VIZ in
 *   oblikovane regijske mreže VIZ v območnih enotah ZRSŠ za prenašanje izkušenj in obetavnih praks.

