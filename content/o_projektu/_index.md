---
title: "O projektu Na-Ma-Poti"
date: 2019-12-04T14:46:31+01:00
draft: true
tags: []
categories: []
slug: o projektu
---

(NAravoslovje, MAtematika, Pismenost, Opolnomočenje, Tehnologija, Interaktivnost)

V projektu bomo analizirali stanje naravoslovne in matematične pismenosti na VIZih. Na osnovi opredeljenih elementov naravoslovne in matematične pismenosti z opisniki bomo razvili in preizkusili didaktične pristope in strategije za vertikalno in horizontalno udejanjanje teh elementov na vseh ravneh znanja v posameznih starostnih obdobjih. Kritično mišljenje v naravoslovni in matematični pismenosti bomo krepili s poudarkom na argumentiranju, metakognitivnem razmišljanju in medijski kritičnosti. Izboljševali bomo strategije interdisciplinarnega reševanja kompleksnih avtentičnih problemov in učenja z raziskovanjem. Premišljeno vključevali in uporabljali IKT za vzpostavitev prožnih in inovativnih učnih okolij, igrifikacijo, programiranje, razvijanje logičnega in algoritmičnega mišljenja. Poudarjali bomo aktivno vlogo vsakega učenca in sodelovanje po načelih formativnega spremljanja ter personalizacijo ter izboljševali odnos učencev do naravoslovja in matematike. Pri projektnih aktivnostih bomo izhajali tudi iz rezultatov in gradiv preteklih projektov s področja naravoslovja in matematike. Vse aktivnosti projekta bodo izhodišče za pripravo priporočil za razvoj naravoslovne in matematične pismenosti v VIZ po vertikali. 