---
title: "Odkrivanje in zaznavanje pojavnih oblik VODE"
date: 2019-12-04
draft: false
trajanje: 4 mesece
avtor: Strokovne delavke Vrtca pri OŠ Renče
obdobja: [vrtec]
gradniki: [np1, np1.1, np1.2, np1.3]
tags: [voda, narava]
---
{{< figure src="/dejavnosti/voda.jpg" title="Otrok spoznava vodo v različnih pojavnih oblikah. S tem doživlja in spoznava neživo naravo v njeni raznolikosti, povezanosti in stalnem spreminjanju." >}}

<!--more-->
## Operativni cilji dejavnosti

* Otrok spoznava vodo v različnih pojavnih oblikah.
* Otrok prepozna in poimenuje vrsto padavine.
* Otrok z uporabo vseh čutil zaznava, preučuje in raziskuje vodo.
* Otrok s svojimi besedami opiše lastnosti vode.
* Otrok ob prikazu opiše pojav izhlapevanja vode.
* Otrok ob prikazu opiše pojav kroženja vode.

## Aktivnosti

* Opazovanje padanja dežja/neviht, nastajanje luž ...
opisovanje in risanje.
Večdnevno opazovanje (in risanja) nastajanja in
izginjanja luž, skakanje po lužah, primerjanje
velikosti in oblike luž.

* Preverjanje otrokovih predstav, izkušenj,
predznanj skozi razgovor.

* ...
## Povezave

* [PDF dokument](/dejavnosti/VRTEC_Odkrivanje_in_zaznavanje_pojavnih_oblik_vode.pdf)