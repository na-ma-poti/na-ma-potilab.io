---
title: "Prvi gradnik matematične pismenosti"
subtitle: "Matematično mišljenje"
date: 2019-12-09
draft: true
tags: []
gradniki: [mp1]
categories: []
---
## Opis
Matematično mišljenje, razumevanje in uporaba matematičnih pojmov, postopkov ter strategij, sporočanje kot osnova matematične pismenosti.

<!--more-->
