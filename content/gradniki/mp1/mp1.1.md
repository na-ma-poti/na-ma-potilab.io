---
title: "Razume sporočila z matematično vsebino"
date: 2019-12-11
draft: true
tags: []
gradniki: [mp1, mp1.1]
categories: []
---
Razume sporočila z matematično vsebino
<!--more-->

## Opisniki

 * razume enostavna, strukturirana in kompleksna sporočila z matematično vsebino
 * uporablja ustrezne bralne strategije pri branju z razumevanjem matematičnih 
 besedil in prireševanju besedilnih nalog
 * iz sporočila z matematično vsebino izlušči (matematične) podatked)
 samostojno pridobi podatke iz verodostojnih virov
