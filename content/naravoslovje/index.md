---
title: "Naravoslovje"
date: 2019-09-11T06:28:14+02:00
draft: false
tags: [ fizika, potresi, Luna]
categories: [naravoslovje, opis]
---

Naravoslovna pismenost zajema posameznikovo naravoslovno znanje, naravoslovne
spretnosti/veščine in odnos do naravoslovja.
<!--more-->
Temelji na uporabi znanja, spretnosti/veščin za: 

* obravnavanje naravoslovno-znanstvenih vprašanj, pridobivanje novega znanja,
* razlaganje naravoslovnih pojavov ter
* izpeljavo ugotovitev o naravoslovnih tematikah, ki temeljijo na podatkih in
  preverjenih dejstvih. 
    
Naravoslovna pismenost vključuje tudi razumevanje značilnosti naravoslovnih
znanosti kot oblike človeškega znanja in raziskovanja, zavedanje o tem, kako
naravoslovne znanosti in tehnologija oblikujejo naše snovno, intelektualno in
kulturno okolje, ter ripravljenost za sodelovanje in zmožnost sporazumevanja o
naravoslovno-znanstvenih vprašanjih kot razmišljujoč in odgovoren posameznik v
odnosu do narave."



## Mesec in potresi

![luna na nebu](luna.jpg)

    Luna vpliva na potrese? 
    
    Kakšne vse neumnosti se ljudje spomnijo. Hmm... zakaj pa 
    je za veliko noč več potresov kot sicer? 
    
    Kako pa bi Luna lahko vplivala na potres?

## Gradniki

## Dejavnosti po obdobjih
