# spletne strani

Spletna stran z gradivi za učitelje in primeri dejavnosti ki spodbujajo razvoj 
naravoslovne in matematične pismenosti na vseh nivojih izobraževanja.

## Kako lahko sodelujem?

 * Javite lahko napake, pripombe ali predloge, tako da odprete  [zahtevek](https://gitlab.com/na-ma-poti/na-ma-poti.gitlab.io/issues/new)
 * Ustvarite dejavnost
 * urejate vsebino
